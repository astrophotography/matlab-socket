# matlab-socket

Creates a communication socket over UDP/IP or Serial port.

## Description

`S = SOCKET` Creates a communication socket over UDP/IP with
default settings. Once initialized, you may communicate via the socket using
the `R = S.CMD(DATA)` combined call, that sends DATA, and gets the response R.

`S = SOCKET('IP:PORT')` Creates a communication socket over UDP/IP with specified IP:PORT target. You may as well pass the IP and PORT as `SOCKET('ip',IP,'port',PORT)` or set the environment variables `SOCKET_IP` and `SOCKET_PORT`. 
The syntax `S = SOCKET('UDP', ...)` specifies explicitly a UDP connection (default).

Once initialized, you may communicate via the socket using
the `R = S.CMD(DATA)` combined call, that sends DATA, and gets the response R.

`S = SOCKET('/dev/ttyS0')` Uses a serial connection instead of UDP/IP
and sets the serial communication port (e.g. `COM1`, `/dev/tty.KeySerial1`).
`S = SOCKET('SERIAL')` will use the first available serial port. You may 
also set the environment variable `SOCKET_SERIAL_DEV`.

`S = SOCKET('property','value',...)` Sets object properties, e.g. the Terminator 
character for communication, as well as the `TimeOut` (in seconds).
For the sake of C/python compatibility, the CONNECT, SEND RECV and CLOSE methods are
defined as aliases to the above calls.

## Usage

```matlab
s = socket('192.168.4.1:11880')
r = s.cmd(':e1')

r =

=0328A5

close(s)
```

## Class Properties


| Property    | description                       |
|-------------|-----------------------------------|
| Terminator  | command terminator                |
| TimeOut     | communication timeout [s]         |
| filenosocket| file descriptor                   |
| ip          | UDP IP:PORT                       |
| last_fread  | last received data                |
| last_fwrite | last sent data                    |
| port        | UDP port                          |
| serial_dev  | serial port                       |
| type        | method to use: 'udp' or 'serial'  |

## Class Methods

 Method | description 
--------|-------------
SOCKET  | Creates a communication socket over UDP/IP or Serial port.
CHAR    | A printable representation of the current socket.
DISPLAY | Print a compact representation of the current socket.
FOPEN   | Open the current socket communication. 
FCLOSE  | Close the current socket communication.
DELETE  | Delete the current socket communication.
FWRITE  | Write data into the current socket communication.
FREAD   | Read data from the current socket communication.
CMD     | Send and retrieve response from the current socket communication.


## Credits
(c) E. Farhi 2024 - GPL3

The UDP communication is performed using `pnet` (Peter Rydesäter - 2015):
- https://github.com/jmcanana/tcp_udp_ip
- https://fr.mathworks.com/matlabcentral/fileexchange/345-tcp-udp-ip-toolbox-2-0-6

