classdef socket < handle
% SOCKET Creates a communication socket over UDP/IP or Serial port.
%
%   S = SOCKET('IP:PORT') Creates a UDP communication socket to IP:PORT. You may as
%   well pass the IP and PORT as SOCKET('ip',IP,'port',PORT) or set the environment
%   variables SOCKET_IP and SOCKET_PORT.
%
%   S = SOCKET('/dev/ttyS0') Uses a serial connection instead of UDP/IP
%   and sets the serial communication port (e.g. COM1, /dev/tty.KeySerial1).
%   S = SOCKET('SERIAL') will use the first available serial port. You may 
%   also set the environment variable SOCKET_SERIAL_DEV.
%
%   R = S.CMD(DATA) Sends DATA, and gets the response R. The data stream is opened
%   if not done yet.
%
%   FOPEN(S)        Opens the data stream.
%   FWRITE(S, DATA) Sends DATA into the socket.
%   R = FREAD(S)    Gets the response R
%   FCLOSE(S)       Closes the data stream.
%
%   S = SOCKET('property','value',...) Sets object properties, e.g. the Terminator 
%   character for communication, as well as the TimeOut (in seconds - recommended).
%   For the sake of C/python compatibility, the CONNECT, SEND RECV and CLOSE 
%   methods are defined as aliases to the above calls.
%
% Example: s=socket('192.168.4.1:11880'); s.fwrite(sprintf(':e1\r')); r=s.fread; s.fclose;
% See also: serial
% (c) E. Farhi 2024 - GPL3 

  properties
    Terminator=sprintf('\r');   % command terminator
    TimeOut   =1;               % communication timeout [s]
  end
  
  properties(SetAccess=protected)
    ip='192.168.4.1:11880';     % UDP IP:PORT
    port        =[];            % UDP port
    serial_dev  ='';            % serial port
    type        ='udp';         % method to use: udp or serial
    last_fread  ='';            % last received data
    last_fwrite ='';            % last sent data
    fileno      =[];            % socket file descriptor
  end
  
  properties(Access=private)
    bufferlength=1024;  % maximum buffer chunks
    opened      =false;
  end
  
  methods
    function obj = socket(varargin)
    % SOCKET Creates a communication socket over UDP/IP or Serial port.
    %   S = SOCKET('IP:PORT')
    %   S = SOCKET('SERIAL_DEVICE')
    
      ip        = getenv('SOCKET_IP');
      port      = str2num(getenv('SOCKET_PORT'));
      serial_dev= getenv('SOCKET_SERIAL_DEV');
      if ~isempty(ip),          obj.ip         = ip;   end
      if ~isempty(port),        obj.port       = port; end
      if ~isempty(serial_dev),  obj.serial_dev = serial_dev; obj.type='serial'; end
      
      g = getports; % available serial ports
      
      % test if first argument is an IP[:PORT]
      for i=1:nargin
        arg1 = varargin{1};
        if ~ischar(arg1), break; end
        if any(strcmpi(arg1, {'udp','serial'})) 
          obj.type   = arg1;
          varargin(1)=[];
        elseif numel(find(arg1 == '.')) == 3 && all(isstrprop(arg1, 'digit') | arg1 == '.' | arg1 == ':')
          obj.ip = arg1;
          varargin(1)=[];
        elseif any(strcmp(arg1, g))
          obj.type       = 'serial';
          obj.serial_dev = arg1;
          varargin(1)=[];
        end
      end
      
      for i=1:2:numel(varargin)
        p = varargin{i};
        if isprop(obj, p)
          if strcmpi(p, 'serial_dev')
            obj.type='serial';
          end
          obj.(p) = varargin{i+1};
        end
      end
      
      % test the pnet availability
      if strcmpi(obj.type, 'udp') && exist('pnet') && exist('pnet') ~= 3
        % try to compile it
        pnet closeall
      end
      if strcmpi(obj.type, 'udp')
        % extract IP:PORT from URL. Test that it can be reached.
        ip = regexp(obj.ip, '([012]?\d{1,2}\.){3}[012]?\d{1,2}','match');
        i  = find(obj.ip == ':', 1, 'last'); % get port from URL ?
        if ~isempty(i)
          obj.port = str2num(obj.ip((i+1):end));
        end
        obj.ip = char(ip);
        ip = java.net.InetAddress.getByName(char(ip));
        if ~ip.isReachable(1000)
          error([ mfilename ': ERROR: Can not connect to ' obj.ip '. Unreachable.' ]) 
        end
      end
      
      switch lower(obj.type)
      case 'udp'
        obj.fileno=pnet('udpsocket', obj.port);
        if obj.fileno == -1
          error([ mfilename ': ERROR: Can not assign UDP socket on port ' obj.port ]);
        end
      case 'serial'
        if isempty(obj.serial_dev)
          if ~isempty(g), 
            disp([ mfilname ': Available serial ports: using first.' ]);
            disp(g);
            obj.serial_dev = g{1};
          else 
            error([ mfilename ': ERROR: Can not find any serial port' ]); 
          end
        end
        if ~isempty(obj.serial_dev)
          obj.fileno = serial(obj.serial_dev);
          obj.fileno.Terminator = obj.Terminator;
        end
      otherwise
        error([ mfilename ': ERROR: Unsupported communication protocol ' obj.type ]);
      end
    end % socket init
    
    function c = char(obj, arg)
    % CHAR A printable representation of the current socket.
    % CHAR(obj, 'short') Returns a short representation.
    
      switch lower(obj.type)
      case 'udp'
        c = [ obj.ip ':' mat2str(obj.port) ' ' upper(obj.type) ];
      case 'serial'
        c = [ obj.serial_dev ' ' upper(obj.type) ];
      end
      if nargin > 1, return; end
      if obj.opened
        fr = obj.last_fread; fw = obj.last_fwrite; 
        fr(fr <32) = ''; fw(fw <32) = '';
        if numel(fr) > 10, fr=fr(1:10); end
        if numel(fw) > 10, fw=fw(1:10); end
        c = [ c ' WRITE="' fw '" READ="' fr '"' ]; 
      else
        c = [ c ' INACTIVE' ];
      end
    end % char
    
    function display(self)
    % DISPLAY Print a compact representation of the current socket.
      if ~isempty(inputname(1))
        iname = inputname(1);
      else
        iname = 'ans';
      end
      if isdeployed || ~usejava('jvm') || ~usejava('desktop') || nargin > 2, id=class(self);
      else id=[  '<a href="matlab:doc ' class(self) '">' class(self) '</a> ' ...
                 '(<a href="matlab:methods ' class(self) '">methods</a>,' ...s
                 '<a href="matlab:disp(' iname ');">more...</a>)' ];
      end
      fprintf(1,'%s = %s %s\n',iname, id, char(self));
    end % display
    
    function set.TimeOut(obj, sec)
    % SET.TimeOut Set the socket time-out (in sec).
      if ~isnumeric(sec) || sec < 0, return; end
      switch lower(obj.type)
      case 'udp'
        pnet(obj.fileno,'setreadtimeout', sec);
        pnet(obj.fileno,'setwritetimeout',sec);
      case 'serial'
        obj.fileno.TimeOut = sec;
      end
      obj.TimeOut = sec;
    end % set.TimeOut
    
    function set.Terminator(obj, value)
    % SET.Terminator Set the socket message Terminator.
      if strcmpi(value, 'CR')     value = sprintf('\r');
      elseif strcmpi(value, 'LF') value = sprintf('\n');
      end
      if numel(value) ~= 1, return; end
      if strcmpi(obj.type,'serial')
        obj.fileno.Terminator = char(value);
      end
      obj.Terminator = char(value);
    end % set.Terminator
    
    function f=fopen(obj)
    % FOPEN Open the current socket communication. 
    % Return the opened stream. This is automatically done when needed (FREAD/FWRITE).
      if obj.opened, return; end
      switch lower(obj.type)
      case 'udp'
        f=pnet(obj.fileno, 'udpconnect', obj.ip, obj.port); % open the connection
        if f==-1, 
          error([ mfilename ': ERROR: Can not open UDP connection ' obj.ip ':' num2str(obj.port) ]); 
        end
        f=[ obj.ip, ':' obj.port ];
        % setTimeOut(obj, 1) to avoid halt when UDP messages are dropped.
        pnet(obj.fileno,'setreadtimeout', 1);
        pnet(obj.fileno,'setwritetimeout',1);
      case 'serial'
        fopen(obj.fileno);
        f=obj.serial_dev;
      end
      obj.opened=true;
    end % fopen
    
    function fclose(obj)
    % FCLOSE Close the current socket communication.
      if ~obj.opened, return; end
      switch lower(obj.type)
      case 'udp'
        pnet(obj.fileno, 'close');
        obj.fileno = [];
      case 'serial'
        fclose(obj.fileno);
      end
      obj.opened=false;
    end % fclose
    
    function delete(obj)
    % DELETE Delete the current socket communication.
      fclose(obj);
      if strcmpi(obj.type,'serial')
        delete(obj.fileno);
      end
    end % delete
    
    function n=fwrite(obj, data)
    % FWRITE Write data into the current socket communication.
    %   N=FWRITE(OBJ, DATA) Sends DATA and returns the sent message length.
      if ~obj.opened, fopen(obj); end
      if data(end) ~= obj.Terminator
        data(end+1) = obj.Terminator;
      end
      switch lower(obj.type)
      case 'udp'
        pnet(obj.fileno, 'write', data);
        n=pnet(obj.fileno, 'writepacket');
      case 'serial'
        n=fprintf(obj.fileno, data);
      end
      if n ~= numel(data)
        warning('pnet:fwrite', [ mfilename ': Wrote only ' num2str(n) ' bytes.' ]);
      else
        obj.last_fwrite = data;
        obj.last_fwrite(obj.last_fwrite == obj.Terminator) =[];
      end
    end % fwrite
    
    function response = fread(obj)
    % FREAD Read data from the current socket communication.
    %   R = FREAD(OBJ) Returns the collected message from the socket.
      if ~obj.opened, fopen(obj); end
      switch lower(obj.type)
      case 'udp'
        n=pnet(obj.fileno, 'readpacket', obj.bufferlength);
        if n <= 0
          warning('pnet:fread', [ mfilename ': Did not read anything.' ]);
        end
        response = pnet(obj.fileno, 'read', obj.bufferlength);
      case 'serial'
        % flush and get results back
        response = '';
        % we wait for output to be available (we know there will be something)
        t0 = clock;
        while etime(clock, t0) < obj.TimeOut && obj.fileno.BytesAvailable==0
          pause(0.1)
        end
        % we wait until there is nothing else to retrieve
        t0 = clock;
        while etime(clock, t0) < obj.TimeOut && obj.fileno.BytesAvailable
          response = [ response fread(obj) ];
          pause(0.1)
        end
      end
      obj.last_fread=response;
      obj.last_fread(obj.last_fread == obj.Terminator) =[];
    end % fread
    
    function [response,n] = cmd(obj, data)
    % CMD Send and retrieve response from the current socket communication.
    %   R = CMD(DATA) Sends DATA and returns response R.
      n        = fwrite(obj, data);
      response = fread(obj);
    end % cmd
    
    % --------------------------------------------------------------------------
    % <socket.h> C convention aliases
    function connect(obj)
    % CONNECT Open the current socket communication (C socket call).
      fopen(obj);
    end
    
    function n=send(obj, data)
    % SEND Write data into the current socket communication (C socket call).
      n=fwrite(obj, data);
    end
    
    function data=recv(obj)
    % RECV Read data from the current socket communication (C socket call).
      data = fread(obj);
    end
    
    function close(obj)
    % CLOSE Close the current socket communication (C socket call).
      fclose(obj);
    end
    
  end % methods
  
  methods (Access=private)
  
    function setTimeOut(obj, sec)
    % setTimeOut Set the socket time-out (in sec).
      if sec < 0, return; end
      switch lower(obj.type)
      case 'udp'
        pnet(obj.fileno,'setreadtimeout', sec);
        pnet(obj.fileno,'setwritetimeout',sec);
      case 'serial'
        obj.fileno.TimeOut = sec;
      end
      obj.TimeOut = sec;
    end
    
  end % methods private
  
end  % classdef
    
